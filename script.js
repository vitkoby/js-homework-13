'use strict';

/* Ответы на теоритичесике вопросы:
1. Метод setInterval предначен для того, чтоб вызвать код через определенный промежуток времени.
Метод setTimeout это функция, по типу таймера указывает через сколько будет выполнена функция обратного вызова.
2. Функция выполнится сразу после выполнения текущего кода.
3. Функцию clearInterval() подчистит таймеры, которые не нужны во избежание багов из-за неожиданного срабатываения функций. */


const sliderLine = document.querySelector('.slider-line');
const slider = document.querySelector('.slider');
const sliderImages = document.querySelectorAll('.slider-images');
const btnStart = document.querySelector('.button-start');
const btnGroup = document.querySelector('.button-group');
const firstBtn = document.querySelector('.btn-first');
const secondBtn = document.querySelector('.btn-second');

let counter = 0;
let sliderWidth = slider.offsetWidth;

btnStart.addEventListener('click', nextSlide);

function nextSlide () {
	counter++;

	if (counter >= sliderImages.length) {
		counter = 0;
	}

	slideOver();

	let timerId =  setTimeout(() => {
		nextSlide();
	}, 3000);
	
	firstBtn.addEventListener('click', e => {
		clearTimeout(timerId);

		secondBtn.addEventListener('click', nextSlide);
	});
}

function slideOver () {
	sliderLine.style.transform = `translateX(${-counter * sliderWidth}px)`;
}

function showBnt () {
	for (let item of btnGroup.children) {
		if (item.classList.contains('hidden')) {
			item.classList.remove('hidden');
		}
	}
}
setTimeout(() => {
	showBnt();
}, 6000);









